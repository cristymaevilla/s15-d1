// OPERATORS:===================================
// -has types that allows different actions

// A-ASSIGNMENT OPERATORS
// (=) Basic assignment operator 
let assignmentNumber=8;

assignmentNumber = assignmentNumber + 2;
console.log ("Result of addition operator: " + assignmentNumber)
// (+=)ADDITION assignment operator
assignmentNumber += 2;
console.log ("Result of addition operator: " + assignmentNumber)
/* subtraction(-=)  multiplication(*=) division(/=)
*/

// B- ARITHMETIC OPERATORS +,/,*,-, %(Modulo)
let x = 1397;
let y = 7831;

let sum = x + y;
console.log ("Result of addition operator: " + sum);

let difference= x-y;
console.log ("Result of subtraction operator: " + difference);
let product = x*y;
console.log ("Result of multiplication operator: " + product);
let quotient= x/y;
console.log ("Result of division operator: " +quotient);
// Modulo: returns the remainder
let modulus= x%y;
console.log ("Result of addition modulus: " + modulus);

// Multiple Operators and Parentheses
let mdas= 1+2-3*4/5;
console.log("Result of mdas operation: " +mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas operation: " +pemdas);

//C- Increment and Decrement
let z= 1;
// Pre-increment (add value right away)
let increment = ++z;
console.log("result of pre-increment: "+increment);
console.log("Result od pre-increment: " +z);
// Post-increment (the result of increment variable is the same with the value of z which is 2, the increased value will only appear in z)
increment= z++;
console.log("result of post-increment: "+increment);
console.log("result of post-increment: "+z);

let decrement = --z;
console.log("result of pre-decrement: "+decrement);
console.log("Result od pre-decrement: " +z);

decrement = z--;
console.log("result of post-decrement: "+decrement);
console.log("Result od post-decrement: " +z);


//D- TYPE COERCION
// -automatic or implicit conversion of values from one date type to another
let numA= "10";
let numB= 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log (numE);

let numF = false + 1;
console.log (numF);

// E-COMPARISON OPERATORS:
let juan = "juan";
//a. (==)Equality Operator-equal or same content vene though they are diff. in types, returns a boolean value:
console.log(1==1);
console.log(1==2);
console.log(1=='1');
console.log(0==false);
console.log ('juan'==juan);

//b. (!=)inequality Operator 
console.log(1!=1);
console.log(1!=2);
console.log(1!='1');
console.log(0!=false);
console.log ('juan'!=juan);

// c. (===),(!==) STRICT Operator: different data types cannot be coerced use
console.log(1===1);
console.log(1==='1');
console.log(0===false);
console.log ('juan'===juan);

//d. LOGICAL Operators
let isLegalAge = true;
let isRegistered = false;
// (&&)Logical AND Operator : Returns true if all operands are true 
let allRequirementsMet = isLegalAge && isRegistered;
console.log ("Result of logical AND operator: " + allRequirementsMet);
// (||)Logical OR Operator : Returns true if one of the operands are true 
let someRequirementsMet = isLegalAge || isRegistered;
console.log ("Result of logical OR operator: " + someRequirementsMet);
// (!)Logical NOT Operator : Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);


// F. (<, >, <=, >= )RELATIONAL OPERATORS 
let a = 5;
let relate = a>8;
console.log(relate);



// SELECTION CONTROL STRUCTURES=====================

// a. IF, ELSE IF Statement
let numG = -1;
/*IF statement: executes a statements if a specified condition is true
SYNTAX:
	if (condition){
	statement/s;
	}
*/
if (numG < 0){
	console.log('Hello');

}
let numH =-1
// ELSE IF clause ; if previous statement are false and if the specified condition is true:
if(numG > 0){
	console.log("Hello");
}
else if (numH == 0){
	console.log("World");
}
else if (numH > 0){
	console.log("Solar System");
}

// ELSE statement:executes if ALL conditions are false, NO CONDITION
else {
	console.log("Try again");
}
// ------------------------------------
/*
Department A = 1-3
Department B = 4-6
Department C = 7-9
*/

let dept = 4;
if (dept >= 1 && dept <= 3){
	console.log ("You're in Department A");
}
else if (dept >= 4 && dept <= 6){
	console.log ("You're in Department B");
}
else if (dept >= 7 && dept <= 9){
	console.log ("You're in Department C");
}
else{
	console.log("Department does not exist");
}
// ------------------------------------
let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return"Not a typhoon yet.";
	}
	else if(windSpeed < 61){
		return"Tropical Depression detected.";
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return"Tropical storm detected.";
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return"Severe tropical storm detected.";
	}
	else {
		return"Typhoon detected.";
	}
}
// Returns the string to the variable message that invoke it....below is the argument
message = determineTyphoonIntensity(66);
console.log(message);


// TRUTHY and FALSY: help us avoid future error
/*TRUTHY - value that's considered true when encountered in Boolean context
FALSY
-false , 0, -0 , "", null, undefined, NaN*/
if (true){
	console.log('Truthy');
}
if (1){
	console.log('Truthy');
}
if (false){
	console.log('falsy')
}
if (0){
	console.log('falsy')
}
if (undefined){
	console.log('falsy')
}


// b. CONDITIONAL TERNARY OPERATOR: single statement operator
/*SYNTAX:
	(expression) ? ifTrue : ifFalse;
	*/
let ternaryResult = (1<18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);
let name;

function isOfLegalAge(){
	name= 'John';
	return 'You are of legal age limit';
}
function isUnderAge(){
	name= 'Jane';
	return 'You are under the age limit';
}
let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge=(age >  18) ? isOfLegalAge() :isUnderAge();
console.log ("Result of Ternary Operator Functions: " + legalAge + ", " + name);
// ---------------OR DO THIS--------------
/*
if (condition({
	statements;
	}
else {
	statement;
	}
*/

// SWITCH STATEMENT=============================
/* SYNTAX:
	switch (expression) { (NOTE:we compare expression to the case)
		case value1: 
			statement/s;
			break;  (note:like a return if the comparison is true)
		case value2:
			statement/s;
			break;
		case valueN:
			statemen/s;
			break;
		default:  (note:like ELSE)
			statement/s;
	}
*/
// toLowerCase()- transforms the text to lower case form
let day = prompt("What day off the week is it today?").toLowerCase();
console.log(day);

switch (day){
	case 'monday':
		console.log ('The color of the day is red');
		break;	
	case 'tuesday':
		console.log ('The color of the day is orange');
		break;
	case 'wednesday':
		console.log ('The color of the day is yellow');
		break;
	case 'thursday':
		console.log ('The color of the day is green');
		break;
	case 'friday':
		console.log ('The color of the day is blue');
		break;
	case 'saturday':
		console.log ('The color of the day is indigo');
		break;
	case 'sunday':
		console.log ('The color of the day is violet');
		break;
	default:
		console.log('Please input a valid day');


}

/*
	CANNOT BE: switch (dept > 6 && dept<9)
*/
// -----------------------------------------------

// TRY CATCH-FINALLY STATEMENT -to test out the codes and catch its error: put it inside TRY then put catch amd finally
// FINNALLY BLOCK - to specify a response/acton that is used to handle/resolve errors

// NOTE:well use the typhoon example above:
function showIntensityAlert (windSpeed){
	try{
		alert(determineTyphoonIntensity(windSpeed))
	}
	catch (error){
		console.log(typeof error);
		// you can use: console.log(error)
	}
	finally {
		alert("Intensity updates will show new alert.");
	}
}
showIntensityAlert(56);
